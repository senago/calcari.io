let blocks = {
        1 : "start",
        2 : "start",
        3 : "start",
    },
    problem = document.getElementById("problem"),
    numbers = [2, 3, 4, 5, 6, 7, 8, 9, 10],
    lines = [
    document.getElementById("line-1"),
    document.getElementById("line-2"),
    document.getElementById("line-3")],
    isChoosed = true,
    score = 0, correct = 0;

window.onload = init();

async function init () {
    let elements = document.getElementsByClassName('block');
    Array.prototype.forEach.call(elements, function (item, i) {
        item.style.top = '0';
        item.textContent = blocks[i+1];
        item.addEventListener("transitionend", whenFinished, false);
    });
    setInterval(function (){
        let elements = document.getElementsByClassName('block');
        Array.prototype.forEach.call(elements, function (item, i) {
            item.style.transition = "top 2s cubic-bezier(0, 0, 1, 1)";
            item.style.top = '80vh';
            item.textContent = blocks[i+1];

        });
    }, 100);
    setInterval(async function () {
        if(isChoosed === false) {
            updateScore(-1);
        }
        isChoosed = false;
        await updateProblem();
        lines.forEach(function (item) {
            item.className = "line";
        });

    }, 2095)

}

function whenFinished(e) {
    e.target.style.transition = "top 0s cubic-bezier(0, 0, 1, 1)";
    e.target.style.top = "0";
}

async function updateProblem() {
    await shuffle(numbers);
    var a = numbers[0],
        b = numbers[1],
        c = numbers[2],
        d = numbers[3];

    problem.innerText = `${a} * ${b}`;
    var right_answer = Math.floor(Math.random() * (4 - 1)) + 1;
    correct = right_answer;
    blocks[right_answer] = a * b;
    if(right_answer == 1) {
        blocks[2] = a * c;
        blocks[3] = b * d;
    } else if(right_answer == 2) {
        blocks[1] = a * c;
        blocks[3] = b * d;
    } else {
        blocks[1] = a * c;
        blocks[2] = b * d;
    }
}

function updateScore(choosed) {
    if (choosed === correct) {
        score += 10;
    } else {
        lives.lives.pop();
    }
    if(lives.lives.length > 0) {
        document.getElementById('score').textContent = score;
    } else {
        window.location.replace(`gameover.html?score=${score}`);
    }
}

function shuffle(array) {
    for(var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

window.onkeydown = function(e) {
    var key = e.keyCode ? e.keyCode : e.which;
    if(isChoosed === false) {
        if (key == 49) {
            if(1 === correct) {
                lines[0].classList.add("chosen-correctly");
            } else {
                lines[0].classList.add("chosen-wrong");
            }
            updateScore(1);
        } else if (key == 50) {
            if(2 === correct) {
                lines[1].classList.add("chosen-correctly");
            } else {
                lines[1].classList.add("chosen-wrong");
            }
            updateScore(2);
        } else if (key == 51) {
            if(3 === correct) {
                lines[2].classList.add("chosen-correctly");
            } else {
                lines[2].classList.add("chosen-wrong");
            }
            updateScore(3);
        }
    }
    isChoosed = true;
};
let lives = new Vue({
    el: "#lives",
    data: {
        lives: [0, 0, 0, 0, 0]
    }
})
